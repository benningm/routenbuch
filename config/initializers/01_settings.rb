class Settings < Settingslogic
  source(
    ENV.fetch('ROUTENBUCH_CONFIG') do
      "#{Rails.root}/config/routenbuch.yml"
    end
  )
  namespace Rails.env
end
