# configure action mailer settings

Rails.application.config.action_mailer.delivery_method = Settings.mail.delivery_method.to_sym
ActionMailer::Base.delivery_method = Settings.mail.delivery_method.to_sym

Rails.application.config.action_mailer.send(
  "#{Settings.mail.delivery_method}_settings=", 
  Settings.mail.settings.symbolize_keys
)
ActionMailer::Base.send(
  "#{Settings.mail.delivery_method}_settings=", 
  Settings.mail.settings.symbolize_keys
)

Rails.application.config.action_mailer.default_url_options = Settings.base_url.symbolize_keys
ActionMailer::Base.default_url_options = Settings.base_url.symbolize_keys

