Rails.application.configure do
  config.active_storage.variable_content_types = \
    Routenbuch.supported_image_types.map do |t|
      Mime::Type.lookup_by_extension(t).to_s
    end.uniq
end
