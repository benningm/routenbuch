require 'simple_navigation_bootstrap/custom_bootstrap'

SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = SimpleNavigationBootstrap::CustomBootstrap
end
