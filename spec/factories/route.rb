FactoryBot.define do
  factory :route do
    name { 'Frankenschnellweg' }
    geo_ref factory: :crag
  end
end
