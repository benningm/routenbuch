FactoryBot.define do
  factory :closure do
    description { 'No climbing here' }
    geo_ref factory: :crag
    regular_start_month { 1 }
    regular_start_day_of_month { 1 }
    regular_end_month { 6 }
    regular_end_day_of_month { 31 }
    kind { 'flexible' }
  end
end
