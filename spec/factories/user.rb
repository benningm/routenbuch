FactoryBot.define do
  factory :user do
    name { 'John Doe' }
    email { 'john.doe@example' }
    password { 'g00dp4$Sw0rT!' }

    factory :admin_user, parent: :user do
      role { 'admin' }
    end

    factory :contributor_user, parent: :user do
      role { 'contributor' }
    end

    factory :guest_user, parent: :user do
      role { 'guest' }
    end
  end
end
