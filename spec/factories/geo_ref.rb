FactoryBot.define do
  factory :country, class: Country do
    name { 'Germany' }
  end
  factory :region, class: Region do
    name { 'Frankenjura' }
    parent factory: :country
  end
  factory :crag, class: Crag, aliases: [:geo_ref] do
    name { 'Rodenstein' }
    parent factory: :region
  end
  factory :sector, class: Sector do
    name { 'Rodenstein - Edelweißpfeiler' }
    parent factory: :crag
  end
end
