FactoryBot.define do
  factory :season_closure do
    closure
    year { 2020 }
    description { 'exception' }
    start_month { 2 }
    start_day_of_month { 1 }
    end_month { 5 }
    end_day_of_month { 15 }
  end
end
