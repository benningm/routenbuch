module Routenbuch
  class JsonFormatter < ActiveSupport::Logger::SimpleFormatter
    def call(severity, timestamp, _progname, message)
      log = {
        '@timestamp': timestamp.utc.iso8601(3),
        severity: severity
      }

      case message
      when Hash
        log.merge!(message)
      else
        log[:message] = message
      end

      "#{log.to_json}\n"
    end
  end
end
