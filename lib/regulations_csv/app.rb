require 'optparse'

module RegulationsCsv
  class App
    def initialize(handle = nil)
      @handle = handle || ARGF
      parse_opts
    end

    def parse_opts
      OptionParser.new do |opts|
        opts.banner = "Usage: import_csv [options] [file]"

        opts.on("-lLEVEL", "--log_level=LEVEL", "Rails logger log_level") do |level|
          Rails.logger.level = level.to_sym
        end
      end.parse!
    end

    def run
      puts "running in rails env: #{Rails.env}"

      @handle.each_line.with_index do |line, line_number|
        next if line_number == 0
        next if line =~ /^\s*$/
        next if line =~ /^\#/

        record = parse_line(line)
        puts "#{line_number}: importing #{record[:geo_ref_name]}..."

        begin
          geo_ref = GeoRef.find_by!(name: record[:geo_ref_name])
          geo_ref_zone = if geo_ref.zone.present?
                           geo_ref.zone.name
                         else
                           'none'
                         end
          csv_zone = if record[:zone].blank?
                       'none'
                     else
                       "Zone #{record[:zone]}"
                     end
          if geo_ref_zone != csv_zone
            puts "zone mismatch for #{record[:geo_ref_name]} #{geo_ref_zone} (db), #{csv_zone} (file)"
          end
          regulation = Regulation.find_by!(abbr: record[:regulation_abbr])
          RegulatedGeoRef.find_or_create_by!(
            geo_ref: geo_ref,
            regulation: regulation,
            reason: record[:regulation_abbr]
          )
        rescue StandardError => e
          puts "failed to import line #{line_number + 1} (#{record[:geo_ref_name]}): #{e}"
        end
      end
    end

    def parse_line(line)
      line.chomp!
      fields = line.split('|')
      {
        geo_ref_name: fields[0],
        zone: fields[1],
        regulation_abbr: fields[2]
      }
    end
  end
end
