class CreateTopos < ActiveRecord::Migration[5.0]
  def change
    create_table :topos do |t|
      t.string :title
      t.string :description
      t.references :target, polymorphic: true
      t.string :background
      t.string :overlay

      t.timestamps
    end
  end
end
