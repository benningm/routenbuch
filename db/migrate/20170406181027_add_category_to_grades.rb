class AddCategoryToGrades < ActiveRecord::Migration[5.0]
  def change
    add_column :grades, :category, :string

    Grade.where("grades.difficulty < ?", 10).each do | grade |
      grade.category = 'easy'
      grade.save!
    end
    Grade.where("grades.difficulty BETWEEN 10 AND 15").each do | grade |
      grade.category = 'medium'
      grade.save!
    end
    Grade.where("grades.difficulty BETWEEN 16 AND 23").each do | grade |
      grade.category = 'hard'
      grade.save!
    end
    Grade.where("grades.difficulty > ?", 23).each do | grade |
      grade.category = 'ultra'
      grade.save!
    end
  end
end
