class AddGenericFlagToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :generic, :boolean, null: false, default: false
    add_column :products, :description, :text
  end
end
