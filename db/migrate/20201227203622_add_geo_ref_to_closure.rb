class AddGeoRefToClosure < ActiveRecord::Migration[6.1]
  def change
    add_reference :closures, :geo_ref, index: true
    add_foreign_key :closures, :geo_refs

    @lost_n_found = Country.find_or_create_by!(
      name: 'Lost and found',
      parent_id: nil
    )

    Closure.where(geo_ref_id: nil).update_all(geo_ref_id: @lost_n_found.id)

    create_table :closures_geo_refs, id: false do |t|
      t.belongs_to :closure
      t.belongs_to :geo_ref
    end
    add_index :closures_geo_refs, [:closure_id, :geo_ref_id], unique: true

    up_only do
      GeoRef.where.not(closure: nil).each do |g|
        g.applicable_closures << g.closure
      end
    end

    remove_column :geo_refs, :closure_id, :bigint
  end
end
