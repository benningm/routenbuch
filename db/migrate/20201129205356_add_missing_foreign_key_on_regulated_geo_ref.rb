class AddMissingForeignKeyOnRegulatedGeoRef < ActiveRecord::Migration[6.0]
  def change
    up_only do
      update 'DELETE FROM regulated_geo_refs WHERE geo_ref_id NOT IN (SELECT ID FROM geo_refs)'
    end
    add_foreign_key :regulated_geo_refs, :geo_refs
    add_foreign_key :regulated_geo_refs, :regulations
    add_foreign_key :regulations, :geo_refs
  end
end
