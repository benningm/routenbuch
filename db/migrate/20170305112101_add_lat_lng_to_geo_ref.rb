class AddLatLngToGeoRef < ActiveRecord::Migration[5.0]
  def change
    add_column :geo_refs, :lat, :decimal, {:precision=>10, :scale=>6}
    add_column :geo_refs, :lng, :decimal, {:precision=>10, :scale=>6}
    add_index  :geo_refs, [:lat, :lng]
  end
end
