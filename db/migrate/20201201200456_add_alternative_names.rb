class AddAlternativeNames < ActiveRecord::Migration[6.0]
  def change
    add_column :geo_refs, :alternative_names, :string, array: true, null: false, default: []
    add_column :routes, :alternative_names, :string, array: true, null: false, default: []
    add_column :first_ascent_people, :alternative_names, :string, array: true, null: false, default: []
  end
end
