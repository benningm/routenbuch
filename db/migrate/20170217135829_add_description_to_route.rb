class AddDescriptionToRoute < ActiveRecord::Migration[5.0]
  def change
    add_column :routes, :description, :string
  end
end
