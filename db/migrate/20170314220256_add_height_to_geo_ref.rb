class AddHeightToGeoRef < ActiveRecord::Migration[5.0]
  def change
    add_column :geo_refs, :height, :integer
    add_column :geo_refs, :orientation, :integer
  end
end
