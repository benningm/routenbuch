class CreateFirstAscentPeople < ActiveRecord::Migration
  def change
    create_table :first_ascent_people do |t|
      t.string :name
      t.date :birth
      t.date :death

      t.timestamps null: false
    end
  end
end
