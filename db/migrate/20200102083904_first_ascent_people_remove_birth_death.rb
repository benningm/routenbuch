class FirstAscentPeopleRemoveBirthDeath < ActiveRecord::Migration[6.0]
  def change
    remove_column :first_ascent_people, :birth, :date
    remove_column :first_ascent_people, :death, :date
  end
end
