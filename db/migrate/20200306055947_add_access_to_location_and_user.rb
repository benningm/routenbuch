class AddAccessToLocationAndUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :access, :string, null: false, default: 'private'
    add_column :users, :ascents_access, :string, null: false, default: 'private'
    add_column :users, :ticklists_access, :string, null: false, default: 'private'
    add_column :geo_refs, :access, :string, null: false, default: 'internal'

    add_index :users, :access
    add_index :users, :ascents_access
    add_index :users, :ticklists_access
    add_index :geo_refs, :access
  end
end
