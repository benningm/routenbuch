class AddInventory < ActiveRecord::Migration[6.0]
  def change
    create_table :vendors do |t|
      t.string :name, null: false
      t.string :url
      t.timestamps
    end

    create_table :products do |t|
      t.string :name, null: false
      t.string :article_number
      t.string :kind, null: false, default: 'glue-in bolt'
      t.string :url
      t.string :standard
      t.belongs_to :vendor
      t.timestamps
    end

    create_table :inventories do |t|
      t.belongs_to :route
      t.integer :anchor_number
      t.belongs_to :product
      t.date :installed_at
      t.date :removed_at
      t.string :description
      t.string :role, null: false, default: ''
      t.timestamps
    end
  end
end
