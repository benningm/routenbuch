class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
      t.string :photo
      t.string :description
      t.references :target, polymorphic: true

      t.timestamps
    end
  end
end
