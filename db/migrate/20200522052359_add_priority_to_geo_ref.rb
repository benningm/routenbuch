class AddPriorityToGeoRef < ActiveRecord::Migration[6.0]
  def change
    add_column :geo_refs, :priority, :integer, null: false, default: 0
    add_index :geo_refs, :priority
  end
end
