require 'csv'

CSV.foreach(
  Rails.root.join('db', 'seeds', 'vendors.csv'),
  headers: true,
  encoding: 'utf-8'
) do |row|
  Vendor.find_or_create_by!(
    name: row['name']
  ) do |v|
    v.url = row['url']
  end
end
