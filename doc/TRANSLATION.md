Update translations with gettext
================================

To find all the translations use:

    rake gettext:find

then update the files in `local/`.
