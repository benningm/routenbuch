class EffectivePermission < ApplicationRecord
  belongs_to :geo_ref
  belongs_to :permission
end
