class ClosureTemplate < ApplicationRecord
  validates(
    :name,
    presence: true,
    uniqueness: { message: _('must be unique') }
  )
  validates :description, presence: true
  validates(
    :regular_start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :regular_start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )
  validates(
    :regular_end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :regular_end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )
end
