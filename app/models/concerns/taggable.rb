module Taggable
  extend ActiveSupport::Concern

  included do
    has_many :assigned_tags, as: :taggable
    has_many :tags, through: :assigned_tags

    validate :validate_tags_model
    def validate_tags_model
      tags.each do |tag|
        unless self.class.name == tag.model
          errors.add(:tags, "Tag #{tag.name} can only be assigned to #{tag.model} objects.")
        end
      end
    end
  end
end
