class SecondaryZone < ApplicationRecord
  belongs_to :geo_ref
  belongs_to :zone

  validates :description, presence: true

  def self.valid_geo_ref_types
    [Crag, Sector]
  end

  def self.valid_geo_ref_type?(klass)
    valid_geo_ref_types.include?(klass)
  end

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if self.class.valid_geo_ref_type?(geo_ref.class)

    errors.add(:geo_ref, _('must be an allowed location type.'))
  end

  def ransackable_attributes(auth_object = nil)
    %w[description]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
