class MonthSelectInput < SimpleForm::Inputs::CollectionInput
  MONTH_COLLECTION = [
    [N_('January'), 1],
    [N_('February'), 2],
    [N_('March'), 3],
    [N_('April'), 4],
    [N_('May'), 5],
    [N_('June'), 6],
    [N_('July'), 7],
    [N_('August'), 8],
    [N_('September'), 9],
    [N_('October'), 10],
    [N_('November'), 11],
    [N_('December'), 12]
  ].freeze

  def month_collection
    MONTH_COLLECTION
  end

  def input(wrapper_options = nil)
    label_method, value_method = detect_collection_methods

    merged_input_options = merge_wrapper_options(input_html_options, wrapper_options)

    @builder.collection_select(
      attribute_name, month_collection.map {|m| [_(m[0]), m[1]] }, value_method, label_method,
      input_options, merged_input_options
    )
  end
end
