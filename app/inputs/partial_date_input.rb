class PartialDateInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    prefix = options[:prefix]
    model = @builder.object_name
    initial_value = @builder.object.send(attribute_name)

    template.content_tag(
      'partial-date-input',
      '',
      prefix: prefix,
      model: model,
      ':initial-year': initial_value[0],
      ':initial-month': initial_value[1],
      ':initial-day': initial_value[2],
    ).html_safe
  end
end
