class TagsInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    name = options[:name]
    name ||= "#{@builder.object_name}[#{attribute_name}][]"
    initial_value = @builder.object.send(attribute_name)

    template.content_tag(
      'tags-input',
      '',
      name: name,
      ':initial-value': initial_value.to_json,
      placeholder: options[:placeholder] || _('Add tag...'),
      'add-button-text': options[:add_button_text] || _('Add'),
    ).html_safe
  end
end
