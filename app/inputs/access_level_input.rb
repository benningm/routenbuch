class AccessLevelInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    levels = @builder.object.class.access_levels
    @builder.select(attribute_name, levels.map { |l| [_(l), l] }.to_h , {}, input_html_options)
  end

  def input_html_options
    super.merge({class: 'form-control'})
  end
end
