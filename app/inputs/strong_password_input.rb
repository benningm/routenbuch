class StrongPasswordInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    name ||= "#{@builder.object_name}[#{attribute_name}]"

    template.content_tag(
      'strong-password-input',
      '',
      name: name,
      'check-url': options[:check_url] || '/password_strength'
    ).html_safe
  end
end
