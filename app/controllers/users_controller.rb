class UsersController < ApplicationController
  before_action :set_user, only: [:show]
  skip_authorization_check :only => [:index]

  def index
    @q = User.ransack(params[:q])
    @users = @q.result.accessible_by(current_ability, :read).page(params[:page])
  end

  def show
    authorize! :read, @user
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:id)
  end
end
