class Admin::ProductsController < Admin::ApplicationController
  before_action :set_product, only: %i[show edit update destroy]
  skip_authorization_check :only => [:index]

  def index
    @q = Product.ransack(params[:q])
    @products = @q.result \
      .accessible_by(current_ability, :admin) \
      .includes(:vendor) \
      .order(created_at: :desc) \
      .page(params[:page])
  end

  def show
    authorize! :admin, @product
  end

  def new
    @product = Product.new
    authorize! :admin, @product
  end

  def edit
    authorize! :admin, @product
  end

  def create
    @product = Product.new(product_params)
    authorize! :admin, @product

    if @product.save
      redirect_to admin_products_path, notice: _('Product was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :admin, @product

    if @product.update(product_params)
      redirect_to admin_product_path(@product), notice: _('Product was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :admin, @product
    @product.destroy
    redirect_to admin_products_url, notice: _('Product was successfully removed.')
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params \
      .require(:product) \
      .permit(
        :name,
        :photo,
        :article_number,
        :kind,
        :url,
        :standard,
        :vendor_id,
        :generic,
        :description
      )
  end
end
