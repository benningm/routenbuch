class InventoriesController < ApplicationController
  before_action :set_inventory, only: [:show, :edit, :update, :destroy, :remove]
  before_action :set_route
  skip_authorization_check only: [:index]

  def index
    @inventories = RouteInventoryPresenter.new(@route, current_ability).inventories
    @route_links_from = @route.route_links_from \
      .accessible_by(current_ability, :read)
    @route_links_to = @route.route_links_to \
      .accessible_by(current_ability, :read)

    @removed_inventories = @route.inventories \
      .where.not(removed_at: nil) \
      .accessible_by(current_ability, :read) \
      .includes(:product) \
      .order(anchor_number: :desc, removed_at: :desc)
  end

  def new
    @inventory = Inventory.new(route: @route)
    if @route.inventories.any?
      latest = @route.inventories.order(created_at: :desc).limit(1).first
      @inventory.anchor_number = if latest.role == 'belay'
                                   latest.anchor_number
                                 else
                                   latest.anchor_number + 1 
                                 end
      @inventory.product = latest.product
      @inventory.installed_at = latest.installed_at
      @inventory.removed_at = latest.removed_at
      @inventory.description = latest.description
      @inventory.role = latest.role
    else
      @inventory.anchor_number = 1
      @inventory.role = 'intermediate anchor'
    end
    authorize! :create, @inventory
  end

  def show
    authorize! :read, @inventory
    @product = @inventory.product
    authorize! :read, @product
  end

  def edit
    authorize! :update, @inventory
  end

  def create
    @inventory = Inventory.new(inventory_params)
    @inventory.route = @route
    authorize! :create, @inventory

    if @inventory.save
      redirect_path = if params.key? 'commit-and-next'
                        new_route_inventory_path(@route)
                      else
                        route_inventories_path(@route)
                      end
      redirect_to redirect_path, notice: _('Inventory was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @inventory

    if @inventory.update(inventory_params)
      redirect_to route_inventories_path(@route), notice: _('Inventory was successfully updated.')
    else
      render :edit
    end
  end

  def remove
    authorize! :update, @inventory
    @inventory.update!(removed_at: Date.today)
    redirect_to route_inventories_path(@route), notice: _('Inventory was successfully updated.')
  end

  def destroy
    authorize! :destroy, @inventory
    @inventory.destroy
    redirect_to route_inventories_path(@route), notice: _('Inventory was successfully removed.')
  end

  private

  def set_inventory
    @inventory = Inventory.find(params[:id])
  end

  def set_route
    @route = Route.find(params[:route_id])
    authorize! :read, @route
  end

  def inventory_params
    params \
      .require(:inventory) \
      .permit(
        :anchor_number,
        :product_id,
        :installed_at,
        :removed_at,
        :description,
        :role
      )
  end
end
