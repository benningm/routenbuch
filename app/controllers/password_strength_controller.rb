class PasswordStrengthController < ApplicationController
  def check
    authorize! :check, :password_strength

    password = params[:password] || ''

    result = case password
             when ''
               {
                 entropy: 0,
                 is_strong: false
               }
             else
               {
                 entropy: checker.calculate_entropy(password).round(2),
                 is_strong: checker.is_strong?(password)
               }
             end

    result[:min_entropy] = checker.min_entropy

    render json: result
  end

  private

  def checker
    @checker ||= StrongPassword::StrengthChecker.new
  end
end
