class ScalesController < ApplicationController
  skip_authorization_check :only => [:table]

  # TODO: logic should be moved to model or presenter?
  def table
    scope = params[:scope]
    @scales = Grade.distinct.where( scope: scope ).accessible_by(current_ability, :read).pluck(:scale)
    scale_index = {}
    @scales.each_with_index do |scale,index|
      scale_index[scale] = index
    end
    @grades = Grade.where( scope: scope )
    @table = []
    @grades.each do |grade|
      if @table[grade.difficulty].nil?
        @table[grade.difficulty] = @scales.map { nil }
      end
      @table[grade.difficulty][scale_index[grade.scale]] = grade
    end
  end
end
