class GeoRefsController < ApplicationController
  before_action(
    :set_geo_ref,
    only: %i[
      show edit update destroy
      map_items
      sort_routes reorder_routes
      sort_childs reorder_childs
      select_items move_items
    ]
  )
  before_action :set_move_items_params, only: %i[select_items move_items]
  skip_authorization_check only: %i[index browse autocomplete]
  back_navigatable_actions %i[index show browse]

  def autocomplete
    @geo_refs = GeoRef \
      .where('name ILIKE (?)', "%#{params[:term]}%") \
      .accessible_by(current_ability, :read) \
      .limit(10)
    render(
      json: @geo_refs.map do |g|
        {
          id: g.id,
          value: g.name,
          label: "#{g.name} (#{g.class.name})",
        }
      end
    )
  end

  def map
    # TODO select items based on map (geokit)
  end

  def map_items
    authorize! :read, @geo_ref
    @childs = @geo_ref.childs.with_location.accessible_by(current_ability, :read)

    @items = if @childs.any?
               @childs
             else
               [@geo_ref]
             end

    render json: GeoRef::MapItemSerializer.new(@items).serializable_hash.to_json
  end

  def browse
    @geo_refs = GeoRef.where(parent: nil).accessible_by(current_ability, :read)
    respond_to do |format|
      format.html 
    end
  end

  def index
    @q = GeoRef.ransack(params[:q])
    @geo_refs = @q.result \
      .accessible_by(current_ability, :read)\
      .order(created_at: :desc) \
      .page(params[:page])
    respond_to do |format|
      format.html 
    end
  end

  def show
    authorize! :read, @geo_ref
    @closures = @geo_ref.applicable_closures.includes(:relevant_season_closures)
    @route_closures = Closure.joins(:routes).where('routes.geo_ref_id': @geo_ref.id)
    @childs = childs
    @routes = routes.includes(:grade, closures: :relevant_season_closures)
    @topos = @geo_ref.topos.accessible_by(current_ability, :read)
  end

  def sort_childs
    authorize! :update, @geo_ref
    @childs = childs(action: :update)
  end

  def reorder_childs
    authorize! :update, @geo_ref
    priorities = params.require(:priorities)

    childs(action: :update).each do |c|
      next unless priorities.key? c.id.to_s

      c.update!(priority: priorities[c.id.to_s])
    end

    render json: {}
  end

  def sort_routes
    authorize! :update, @geo_ref
    @routes = routes(action: :update)
  end

  def reorder_routes
    authorize! :update, @geo_ref
    priorities = params.require(:priorities)

    routes(action: :update).each do |r|
      next unless priorities.key? r.id.to_s

      r.update!(priority: priorities[r.id.to_s])
    end

    render json: {}
  end

  def new
    @geo_ref = GeoRef.new(params.permit(:type))

    if params.key? :parent_id
      parent = GeoRef.find(params[:parent_id])
      @geo_ref.parent = parent
      @geo_ref.access = parent.access
    end

    authorize! :create, @geo_ref
    render :new
  end

  def edit
    authorize! :update, @geo_ref
  end

  def create
    @geo_ref = GeoRef.new(geo_ref_params)
    authorize! :create, @geo_ref
    
    @geo_ref.copy_parent_effective_permissions if @geo_ref.parent.present?

    respond_to do |format|
      if @geo_ref.save
        format.html { redirect_to geo_ref_path(@geo_ref), notice: _('Geo ref was successfully created.') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize! :update, @geo_ref
    respond_to do |format|
      if @geo_ref.update(geo_ref_params)
        format.html { redirect_to geo_ref_path(@geo_ref), notice: _('Geo ref was successfully updated.') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    authorize! :destroy, @geo_ref
    @geo_ref.destroy
    respond_to do |format|
      redirect_url = if @geo_ref.parent.present?
                       geo_ref_path(@geo_ref.parent)
                     else
                       geo_refs_path
                     end
      format.html { redirect_to redirect_url, notice: _('Geo ref was successfully destroyed.') }
    end
  end

  def select_items
    authorize! :read, @geo_ref
  end

  def move_items
    authorize! :read, @geo_ref

    new_geo_ref = @move_items_params.new_geo_ref
    @move_items_params.selected_routes.each do |route|
      route.geo_ref = new_geo_ref
      route.skip_geo_ref_update_stats = true
    end
    @move_items_params.selected_geo_refs.each do |geo_ref|
      geo_ref.parent = new_geo_ref
      geo_ref.skip_parent_changed_validation = true
    end

    if @move_items_params.valid?
      authorize! :read, new_geo_ref

      @move_items_params.selected_routes.each do |route|
        authorize! :create, route
        route.save!
      end
      @move_items_params.selected_geo_refs.each do |geo_ref|
        authorize! :create, geo_ref
        geo_ref.save!
      end

      new_geo_ref.update_self_and_parents_stats
      @geo_ref.update_self_and_parents_stats

      new_geo_ref.rebuild_effective_permissions \
        if @move_items_params.selected_geo_refs.any?
    
      redirect_to geo_ref_path(@geo_ref), notice: _('Child items have been moved to new location.')
    else
      select_items
      render :select_items
    end
  end

  private

  def routes(action: :read, order: [:priority, :name])
    @geo_ref.routes.accessible_by(current_ability, action).order(order)
  end

  def childs(action: :read, order: [:priority, :name])
    @geo_ref.childs.accessible_by(current_ability, action).order(order)
  end

  def set_geo_ref
    @geo_ref = GeoRef.find(params[:id])
  end

  def geo_ref_type
    params.require(:geo_ref).permit(:type)
  end

  def geo_ref_params
    p = params.require(:geo_ref).permit(
      :name,
      :type,
      :parent_id,
      :description,
      :body,
      :lat,
      :lng,
      :zone_id,
      :height,
      :orientation,
      :access,
      tag_ids: [],
      alternative_names: []
    )
    p[:alternative_names]&.reject!(&:blank?)
    p
  end

  def geo_ref_move_items_params
    params.fetch(:geo_refs_controller_move_items_params, {}).permit(
      :new_geo_ref_id,
      route_ids: [],
      geo_ref_ids: []
    )
  end

  def set_move_items_params
    @move_items_params = GeoRefsController::MoveItemsParams.new
    @move_items_params.new_geo_ref = GeoRef.find(
      geo_ref_move_items_params[:new_geo_ref_id]
    ) unless geo_ref_move_items_params[:new_geo_ref_id].blank?

    @move_items_params.routes = routes(action: :destroy)
    @move_items_params.selected_routes = routes(action: :destroy) \
      .where(id: geo_ref_move_items_params[:route_ids].to_a.reject(&:blank?))

    @move_items_params.geo_refs = childs(action: :destroy)
    @move_items_params.selected_geo_refs = childs(action: :destroy) \
      .where(id: geo_ref_move_items_params[:geo_ref_ids].to_a.reject(&:blank?))
  end
end
