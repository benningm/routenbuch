class SecondaryZonesController < ApplicationController
  before_action :set_geo_ref
  before_action :set_secondary_zone, only: [:edit, :update, :destroy]

  def new
    @secondary_zone = SecondaryZone.new
    @secondary_zone.geo_ref = @geo_ref
    authorize! :create, @secondary_zone
  end

  def edit
    authorize! :update, @secondary_zone
  end

  def create
    @secondary_zone = SecondaryZone.new(secondary_zone_params)
    @secondary_zone.geo_ref = @geo_ref
    authorize! :create, @secondary_zone
    if @secondary_zone.save
      redirect_to @geo_ref.as_base_class, notice: _('SecondaryZone was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @secondary_zone
    if @secondary_zone.update(secondary_zone_params)
      redirect_to @secondary_zone.geo_ref.as_base_class, notice: _('SecondaryZone was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @secondary_zone
    @secondary_zone.destroy
    redirect_to @secondary_zone.geo_ref.as_base_class, notice: _('SecondaryZone was successfully destroyed.')
  end

  private

  def set_geo_ref
    @geo_ref = GeoRef.find(params[:geo_ref_id])
  end

  def set_secondary_zone
    @secondary_zone = SecondaryZone.find(params[:id])
  end

  def secondary_zone_params
    params.require(:secondary_zone).permit(
      :description,
      :zone_id,
    )
  end
end
