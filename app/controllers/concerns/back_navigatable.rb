require 'active_support/concern'

module BackNavigatable
  extend ActiveSupport::Concern

  included do
    before_action :log_back_path
    helper_method :back_path
  end

  class_methods do
    def back_navigatable_actions(actions = nil)
      @back_navigatable_actions = actions.map(&:to_s) unless actions.nil?
      @back_navigatable_actions || ['show', 'index']
    end
  end

  def log_back_path
    return unless self.class.back_navigatable_actions.include?(action_name)

    session[:back_path] = request.env['ORIGINAL_FULLPATH']
  end

  def back_path
    session[:back_path]
  end
end
