class ClosuresController
  class AddRouteParams
    include ActiveModel::Validations
    include ActiveModel::Conversion

    attr_accessor :closure, :route

    validates :closure, presence: true
    validates :route, presence: true

    validate :route_uniqueness

    def route_uniqueness
      return if closure.nil?

      if closure.route_ids.include?(route.id)
        errors.add(:route, _('is already assigned to this closure'))
      end
    end

    def persisted?
      false
    end
  end
end
