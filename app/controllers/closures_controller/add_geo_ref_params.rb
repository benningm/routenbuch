class ClosuresController
  class AddGeoRefParams
    include ActiveModel::Validations
    include ActiveModel::Conversion

    attr_accessor :closure, :geo_ref

    validates :closure, presence: true
    validates :geo_ref, presence: true

    validate :geo_ref_uniqueness

    def geo_ref_uniqueness
      return if closure.nil?
      return if geo_ref.nil?

      if closure.geo_ref_ids.include?(geo_ref.id)
        errors.add(:geo_ref, _('is already assigned to this closure'))
      end
    end

    def persisted?
      false
    end
  end
end
