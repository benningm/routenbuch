class RouteInventoryPresenter
  attr_reader :route
  attr_reader :current_ability

  def initialize(route, current_ability)
    @route = route
    @current_ability = current_ability
  end

  def inventories
    result = route_inventories
    route.route_links_to.where.not(anchor_number: nil).each do |route_link|
      result += inventories_for_route_link(route_link)
    end

    result.sort { |a, b| b[:anchor_number] <=> a[:anchor_number] }
  end

  def inventories_for_route_link(route_link)
    result = route_link.inventories \
      .where(removed_at: nil) \
      .accessible_by(current_ability, :read) \
      .includes(:product)

    result.to_a.map do |i|
      anchor_number = case route_link.kind
                      when 'start_with'
                        i.anchor_number
                      when 'belay', 'cross'
                        route_link.anchor_number
                      when 'end_with', 'overlap'
                        i.anchor_number + (route_link.anchor_number - route_link.first_anchor_number)
                      end
      {
        anchor_number: anchor_number,
        show_route: true,
        item: i
      }
    end
  end

  def route_inventories
    result = @route.inventories \
      .where(removed_at: nil) \
      .accessible_by(current_ability, :read) \
      .includes(:product)

    result.to_a.map do |i|
      {
        anchor_number: i.anchor_number,
        show_route: false,
        item: i
      }
    end
  end
end
